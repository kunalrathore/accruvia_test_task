from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from api.models import UserTweets
from api.constant import MESSAGE, CREATE_TWEET
from api.serializers import TweetGetSerializer, TweetPostSerializer


class TweetView(APIView):
    """
    Create tweet.
    """

    def post(self, request):
        serializer = TweetPostSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            response_data = {MESSAGE: CREATE_TWEET}
            return Response(response_data, status.HTTP_201_CREATED)

    #  Featch all tweets.
    def get(self, request):
        tweets = UserTweets.objects.order_by("-created_at", "username")
        serializer = TweetGetSerializer(tweets, many=True)
        return Response(serializer.data, status.HTTP_200_OK)
